{
  description = "Flake for developing pirates ums";
  # Flake inputs
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  # Flake outputs
  outputs =
    { self, ... }@inputs:
    let
      forAllSystems = inputs.nixpkgs.lib.genAttrs [
        "aarch64-linux"
        # "i686-linux"
        "x86_64-linux"
        "aarch64-darwin"
        "x86_64-darwin"
      ];
      treefmtEval = forAllSystems (
        system:
        let
          pkgs = inputs.nixpkgs.legacyPackages.${system};
        in
        inputs.treefmt-nix.lib.evalModule pkgs {
          # Used to find the project root
          config = {
            projectRootFile = "flake.nix";
            programs = {
              # Nix formatters
              nixfmt-rfc-style.enable = true;
              deadnix.enable = true;
              statix.enable = true;
              # Python
              ruff = {
                check = true;
                format = true;
              };
              # A lot of misc stuff
              prettier = {
                enable = true;
                includes = [
                  "*.cjs"
                  "*.css"
                  # "*.html" # Does some strange formatting to html template files, hence using djlint as below instead
                  "*.js"
                  "*.json"
                  "*.json5"
                  "*.jsx"
                  "*.md"
                  "*.mdx"
                  "*.mjs"
                  "*.scss"
                  "*.ts"
                  "*.tsx"
                  "*.vue"
                  "*.yaml"
                  "*.yml"
                ];
              };
            };
            # A non upstream formatter for html template files
            settings = {
              global.excludes = [
                "*/static/bootstrap/*" # Should not reformat minified js
                "thirdparty/*"
              ];
              formatter = {
                "djlint" = {
                  command = "${pkgs.djlint}/bin/djlint";
                  options = [ "--reformat" ];
                  includes = [ "*.html" ];
                };
                "typos" = {
                  command = "${pkgs.typos}/bin/typos";
                  options =
                    let
                      configFormat = pkgs.formats.toml { };

                      configFile = configFormat.generate "typos.toml" {
                        default = {
                          extend-ignore-identifiers-re = [
                            # to fix autocorrection in init_db_command STRIPE_MEMBERSHIP_PRODUCT_SKU, line 113
                            "price_1*"
                          ];
                        };
                      };
                    in
                    [
                      "-w"
                      "--config"
                      "${configFile}"
                      "-"
                    ];
                  includes = [
                    "*.cjs"
                    "*.css"
                    "*.html"
                    "*.js"
                    "*.json"
                    "*.json5"
                    "*.jsx"
                    "*.md"
                    "*.mdx"
                    "*.mjs"
                    "*.scss"
                    "*.ts"
                    "*.tsx"
                    "*.vue"
                    "*.yaml"
                    "*.yml"
                    "*.py"
                    "*.pyi"
                    "*.nix"
                  ];
                };
                ruff-format = {
                  # For some god awful reason ruff can only take arguments before a command
                  # IE `ruff format --fix ./file.py` does not work, only `ruff --fix format ./file.py`
                  # So we have to use a force here to clear the set and apply the args in the right order
                  options = inputs.nixpkgs.lib.mkForce [
                    "--fix"
                    "--config"
                    "lint.ignore = [\"E902\"]"
                    "format"
                  ];
                };
                ruff-check = {
                  # For some god awful reason ruff can only take arguments before a command
                  # IE `ruff format --fix ./file.py` does not work, only `ruff --fix format ./file.py`
                  # So we have to use a force here to clear the set and apply the args in the right order
                  options = inputs.nixpkgs.lib.mkForce [
                    "--config"
                    "lint.ignore = [\"E902\"]"
                    "check"
                  ];
                };
              };
            };
          };
        }
      );
    in
    {
      # Development environment output
      devShells = forAllSystems (
        system:
        let
          pkgs = inputs.nixpkgs.legacyPackages.${system};
        in
        ## This provides a convenience script that sets up the required folders for mysql and then launches it.
        {
          default = pkgs.mkShell {
            # The Nix packages provided in the environment
            packages = [
              pkgs.python311
              pkgs.python311Packages.pip
              pkgs.postgresql # Not used for the sql database, but required to build some of the python packages, for some reason.
              pkgs.sqlite # Other sql servers can be used, but sqlite:///:memory: is very convenient
              pkgs.pkg-config
            ];
            shellHook = ''
              python -m venv .venv
              source .venv/bin/activate
              pip install -r requirements.txt
            '';
          };
        }
      );
      formatter = forAllSystems (
        system:
        let
          pkgs = inputs.nixpkgs.legacyPackages.${system};
        in
        treefmtEval.${pkgs.system}.config.build.wrapper
      );
      # for `nix flake check`
      checks = forAllSystems (
        system:
        let
          pkgs = inputs.nixpkgs.legacyPackages.${system};
        in
        {
          formatting = treefmtEval.${pkgs.system}.config.build.check self;
        }
      );
    };
}
