from typing import List
from emma.model.account import Account
import datetime
import time

from app.application import app

from app.models.user_models import User

CONFIG = app.config["MYEMMA"]

myemma_account = Account(
    account_id=CONFIG["account_id"],
    public_key=CONFIG["public_key"],
    private_key=CONFIG["private_key"],
)

# the actual rate limit is 180, use 100 requests a minute as a lower safer bound
MYEMMMA_RATE_LIMIT = 100


def send_catchup_mailings(users: List[str], max_email_age=datetime.timedelta(days=10)):
    """
    Send catch up emails to anyone that just signed up now
    """
    # get all regular mailings that are either being sent out or already complete
    mailings = myemma_account.mailings.fetch_all(
        mailing_types="m", mailing_statuses="s,c"
    )

    # get the latest one (by ID)
    # and then check if it's been less than 10 days since it was sent
    latest_mailing_id = list(mailings)[-1]
    latest_mailing = mailings[latest_mailing_id]
    time_since = datetime.datetime.now() - latest_mailing.get(
        "send_started", datetime.datetime.now()
    )

    if time_since < max_email_age and not latest_mailing.is_archived():
        latest_mailing.send_additional(recipient_emails=users)
    else:
        return None


def _get_users_on_myemma(group=None) -> List[str]:
    members_source = myemma_account.members
    if group:
        members_source = group.members

    members = members_source.fetch_all(deleted=False)
    return [member["email"].lower() for member in members.values()]


def _get_users_to_add(group=None) -> List[str]:
    """
    Get list of all signed up users that haven't been added to myemma
    """
    users_on_my_emma = _get_users_on_myemma(group)
    users = User.query.all()

    missing_users = []

    for user in users:
        if not user.fully_paid:
            continue

        email = user.email.lower()
        if email not in users_on_my_emma:
            missing_users.append(email)

    return missing_users


def get_group_for_mail() -> int:
    """
    Get the largest group on myEmma or generate a new one
    """
    #
    current_groups = myemma_account.groups.fetch_all(group_types="g")
    largest_group_id = None
    largest_group_size = 0
    for id, group in current_groups.items():
        group_dict = dict(group)
        if "active_count" not in group_dict:
            continue
        if group_dict["active_count"] > largest_group_size:
            largest_group_id = id
    if largest_group_id is None:
        # create a new group
        new_group = myemma_account.groups.factory(
            {"group_name": "Pirates Website Contacts", "group_types": "g"}
        )
        myemma_account.groups.save([new_group])
        # get the group using the API
        current_groups = myemma_account.groups.fetch_all(group_types="g")
        newest_group = list(current_groups)[-1]
        #
        return newest_group
    #
    return largest_group_id


def update_users() -> List[str]:
    mail_group_id = get_group_for_mail()
    mail_group = myemma_account.groups.find_one_by_group_id(mail_group_id)

    user_emails = _get_users_to_add(mail_group)

    # this would be the better way to implement it, but the bulk endpoint doesn't appear to support disabling tracking :/
    """
    members_to_add = []
    for email in user_emails:
        # add user with tracking disabled (doesn't currently work with the bulk endpoint, todo fix this)
        members_to_add.append(myemma_account.members.factory({"email": email, "subscriber_consent_tracking" : False}))

    myemma_account.members.save(
        members=members_to_add, filename="website_sync", group_ids=[mail_group_id]
    )
    """

    # add users one by one
    # making sure to keep the update rate reasonable
    # _add_single_user uses cached field names so we use one API call per user added
    start_time = datetime.datetime.now()
    api_call_counter = 0
    for email in user_emails:
        _add_single_user(email, mail_group_id)

        api_call_counter += 1
        time_since_start = datetime.datetime.now() - start_time

        ## not sure if this matches how myemma calculates the rate limit
        ## we just try to not hit the limit by setting our internal limit lower than the API limit

        # reset the rate limit after a minute
        if time_since_start.total_seconds() > 60:
            api_call_counter = 0
            start_time = datetime.datetime.now()
        elif api_call_counter > MYEMMMA_RATE_LIMIT: # rate limit our calls
            time.sleep(60 - time_since_start.total_seconds() + 5) # wait out the remainder of the minute + at least 5 seconds
            api_call_counter = 0
            start_time = datetime.datetime.now()


    

    send_catchup_mailings(user_emails)

    return user_emails

def _add_single_user(user_email: str, mail_group_id: int):
    # add user with tracking disabled
    # if we don't set anything it defaults to enabling it
    member = myemma_account.members.factory({"email": user_email, "subscriber_consent_tracking" : False})
    member.save(group_ids=[mail_group_id])


def add_single_user(user_email: str):
    mail_group_id = get_group_for_mail()
    _add_single_user(user_email, mail_group_id)
