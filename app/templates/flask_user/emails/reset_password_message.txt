{% extends 'flask_user/emails/base_message.txt' %}

{% block message %}
Welcome to Pirates!

Please click the link below to set the password for your Pirates VPN account:
    {{ reset_password_link }}

{% endblock %}
