import datetime
from typing import Callable
from flask import (
    Response,
    flash,
    make_response,
    redirect,
    render_template,
    request,
)
from flask_user import UserManager
from .constants import EMAIL_COOKIE_SIGNED_TOKEN, EMAIL_COOKIE_TTL, EMAIL_COOKIE_TOKEN


class CustomUserManager(UserManager):
    def __init__(
        self,
        app,
        db,
        UserClass,
        registration_form,
        oauth: Callable[[str, str | None], Response],
        **kwargs,
    ):
        self.redirect_to_oauth = oauth
        self.registration_form = registration_form
        super().__init__(app, db, UserClass, **kwargs)

    def customize(self, app):
        self.RegisterFormClass = self.registration_form

    def get_signed_email(self) -> str | None:
        if EMAIL_COOKIE_SIGNED_TOKEN in request.cookies.keys():
            result = self.verify_token(
                request.cookies[EMAIL_COOKIE_SIGNED_TOKEN], EMAIL_COOKIE_TTL.seconds
            )
            if not result:
                return None
            token_id, user_email_preverified = result
            if token_id == EMAIL_COOKIE_SIGNED_TOKEN:
                return user_email_preverified
        return None

    def request_password_reset(self, email):
        """
        Request a password reset for a given email
        """
        user, user_email = self.db_manager.get_user_and_user_email_by_email(email)

        if user and user_email:
            # Send reset_password email
            self.email_manager.send_reset_password_email(user, user_email)

    def logout_view(self):
        resp = make_response(super().logout_view())

        # clear email tokens
        resp.set_cookie(EMAIL_COOKIE_SIGNED_TOKEN, "", 0)
        resp.set_cookie(EMAIL_COOKIE_TOKEN, "", 0)

        return resp

    def login_view(self):
        signed_email = self.get_signed_email()
        if signed_email:
            user, _ = self.db_manager.get_user_and_user_email_by_email(signed_email)
            if user:
                safe_next_url = self._get_safe_next_url(
                    "next", self.USER_AFTER_LOGIN_ENDPOINT
                )
                return self._do_login_user(user, safe_next_url)  # auto-login

        return super().login_view()

    def register_view(self):
        """
        Display registration form and create new User.

        Modified to support OAUTH
        """

        safe_next_url = self._get_safe_next_url("next", self.USER_AFTER_LOGIN_ENDPOINT)
        safe_reg_next_url = self._get_safe_next_url(
            "reg_next", self.USER_AFTER_REGISTER_ENDPOINT
        )

        login_form = self.LoginFormClass(request.form)  # for login.html
        register_form = self.RegisterFormClass(
            request.form
        )  # for login_or_register.html

        preverified_email = self.get_signed_email()

        # setup form
        if request.method != "POST":
            login_form.next.data = register_form.next.data = safe_next_url
            login_form.reg_next.data = register_form.reg_next.data = safe_reg_next_url

            # check if the email token is already in use, in that case just log the user in
            if preverified_email and not self.email_is_available(preverified_email):
                user, _ = self.db_manager.get_user_and_user_email_by_email(
                    preverified_email
                )
                if user:
                    return self._do_login_user(user, safe_reg_next_url)  # auto-login

            # if we have an email token, make the field read-only and fill it in
            if preverified_email:
                register_form.email.data = preverified_email
                if register_form.email.render_kw:
                    register_form.email.render_kw.update({"readonly": True})
                else:
                    register_form.email.render_kw = {"readonly": True}

        if request.method == "POST" and register_form.validate():
            if self.email_is_available(register_form.email.data):
                user = self.db_manager.add_user()
                register_form.populate_obj(user)
                user_email = self.db_manager.add_user_email(user=user, is_primary=True)
                register_form.populate_obj(user_email)

                # Store password hash instead of password
                user.password = self.hash_password(user.password)

                request_email_confirmation = True
                if preverified_email and preverified_email == register_form.email.data:
                    request_email_confirmation = False

                if not request_email_confirmation:
                    if self.db_manager.UserEmailClass:
                        user_email.email_confirmed_at = datetime.datetime.utcnow()
                    else:
                        user.email_confirmed_at = datetime.datetime.utcnow()

                self.db_manager.save_user_and_user_email(user, user_email)
                self.db_manager.commit()

                # Send 'registered' email and delete new User object if send fails if it was needed
                if self.USER_SEND_REGISTERED_EMAIL:
                    try:
                        # Send 'confirm email' or 'registered' email
                        self._send_registered_email(
                            user, user_email, request_email_confirmation
                        )
                    except Exception:
                        if request_email_confirmation:
                            # delete new User object if send  fails
                            self.db_manager.delete_object(user)
                            self.db_manager.commit()
                            raise
                        else:
                            pass
            else:  # email already in use
                # request email
                request_email_confirmation = True
                if preverified_email and register_form.email.data == preverified_email:
                    request_email_confirmation = False
                    user, user_email = self.db_manager.get_user_and_user_email_by_email(
                        preverified_email
                    )
                    return self._do_login_user(user, safe_reg_next_url)  # auto-login
                else:
                    self.request_password_reset(register_form.email.data)
                    # emulate a successful signup
                    flash(
                        f"A confirmation email has been sent to {register_form.email.data} with instructions to complete your registration.",
                        "success",
                    )

            if not request_email_confirmation:
                if "reg_next" in request.args:
                    safe_reg_next_url = self.make_safe_url(register_form.reg_next.data)
                else:
                    safe_reg_next_url = self._endpoint_url(
                        self.USER_AFTER_CONFIRM_ENDPOINT
                    )
                return self._do_login_user(user, safe_reg_next_url)  # auto-login
            else:
                safe_reg_next_url = self.make_safe_url(register_form.reg_next.data)
                return redirect(safe_reg_next_url)

        # Render form
        self.prepare_domain_translations()
        return render_template(
            "flask_user/register.html",
            form=register_form,
            login_form=login_form,
            register_form=register_form,
        )
