# Copyright 2014 SolidBuilds.com. All rights reserved
#
# Authors: Ling Thio <ling.thio@gmail.com>

import enum
from flask_user import UserMixin
from flask_user.forms import RegisterForm
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, BooleanField, validators
from app.application import db


class PaymentType(enum.Enum):
    system = 0  # reserved for the admin account
    card = 1
    admin = 2


# Define the User data model. Make sure to add the flask_user.UserMixin !!
class User(db.Model, UserMixin):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)

    # User authentication information (required for Flask-User)
    email = db.Column(db.Unicode(255), nullable=False, server_default="", unique=True)
    email_confirmed_at = db.Column("confirmed_at", db.DateTime())
    password = db.Column(db.String(255), nullable=False, server_default="")
    # reset_password_token = db.Column(db.String(100), nullable=False, server_default='')
    active = db.Column(db.Boolean(), nullable=False, server_default="0")

    # User information
    active = db.Column("is_active", db.Boolean(), nullable=False, server_default="0")
    first_name = db.Column(db.Unicode(50), nullable=False, server_default="")
    last_name = db.Column(db.Unicode(50), nullable=False, server_default="")

    # Relationships
    roles = db.relationship(
        "Role", secondary="users_roles", backref=db.backref("users", lazy="dynamic")
    )

    # Membership Info
    fully_paid = db.Column(db.Boolean(), nullable=False, server_default="0")
    last_payment_date = db.Column(db.DateTime())
    last_payment_type = db.Column(db.Enum(PaymentType))
    gratis_coupon_id = db.Column(
        db.Integer, db.ForeignKey("gratis_coupon.id", ondelete="SET NULL")
    )  # are they getting free access? (without being a member)

    @property
    def is_admin(self):
        admin_role = Role.query.filter_by(name="admin").first()
        return admin_role in self.roles


# Define the Role data model
class Role(db.Model):
    __tablename__ = "roles"
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(
        db.String(50), nullable=False, server_default="", unique=True
    )  # for @roles_accepted()
    label = db.Column(db.Unicode(255), server_default="")  # for display purposes


# Define the UserRoles association model
class UsersRoles(db.Model):
    __tablename__ = "users_roles"
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey("users.id", ondelete="CASCADE"))
    role_id = db.Column(db.Integer(), db.ForeignKey("roles.id", ondelete="CASCADE"))


# Define the User registration form
# It augments the Flask-User RegisterForm with additional fields
class MyRegisterForm(RegisterForm):
    def validate_email(form, field):
        if not field.data.endswith("@tcd.ie"):
            raise validators.ValidationError(
                "You must have a valid @tcd.ie email address to signup."
            )

    first_name = StringField(
        "First name", validators=[validators.DataRequired("First name is required")]
    )
    last_name = StringField("Last name")
    email = StringField(
        "Email (must be @tcd.ie)",
        validators=[
            validators.DataRequired("Email is required"),
            validators.Email("Invalid email address"),
            validate_email,
        ],
    )


# Define the User profile form
class UserProfileForm(FlaskForm):
    first_name = StringField(
        "First name", validators=[validators.DataRequired("First name is required")]
    )
    last_name = StringField(
        "Last name",
    )
    submit = SubmitField("Save")


class EditUserForm(FlaskForm):
    first_name = StringField(
        "First name", validators=[validators.DataRequired("First name is required")]
    )
    last_name = StringField("Last name")
    email = StringField("Email")
    fully_paid = BooleanField("Fully paid up?")
    submit = SubmitField("Save")


class AddUserForm(FlaskForm):
    first_name = StringField(
        "First name", validators=[validators.DataRequired("First name is required")]
    )
    last_name = StringField("Last name")
    email = StringField(
        "Email", validators=[validators.DataRequired("Last name is required")]
    )
    fully_paid = BooleanField("Paid €2?", default=True)
    submit = SubmitField("Save")


class EditOrCreateOffer(FlaskForm):
    name = StringField("Name", validators=[validators.DataRequired("Name is required")])
    description = StringField("Description")
    resumable = BooleanField("Can offer be resumed after payment?")
    total_days = StringField("Total offer days")
    submit = SubmitField("Save")


class DeleteUserConfirmForm(FlaskForm):
    confirm = BooleanField(
        "Sure?",
        default=False,
        validators=[validators.DataRequired("Must check this box to confirm delete")],
    )
    submit = SubmitField("Delete")


class StripePaymentForm(FlaskForm):
    pass
