from flask_wtf import FlaskForm

from app.application import db
from sqlalchemy.sql import func
from sqlalchemy import (
    Column,
    Integer,
    DateTime,
    Unicode,
    ForeignKey,
    UnicodeText,
    Boolean,
)


class Config(db.Model):
    __tablename__ = "config"
    key = Column(Unicode(255), primary_key=True)
    value = Column(Unicode(255))


class Offer(db.Model):
    __tablename__ = "gratis_offer"
    id = Column(Integer, primary_key=True)
    creation_time = Column(
        "creation_time", DateTime(timezone=True), server_default=func.now()
    )
    # human readable info
    name = Column(Unicode(255), nullable=False)
    description = Column(UnicodeText())
    ### config
    # can the offer be resumed if the user pays for full membership before it's completed?
    resumable = Column(Boolean(), nullable=False)
    total_days = Column(Integer, nullable=False)
    not_before = Column(
        DateTime(timezone=True), server_default=func.now(), nullable=False
    )  # not valid before
    not_after = Column(DateTime(timezone=True), nullable=True)  # not valid after


class Coupon(db.Model):
    __tablename__ = "gratis_coupon"
    id = Column(Integer, primary_key=True)
    gratis_offer_id = Column(
        Integer, ForeignKey("gratis_offer.id", ondelete="CASCADE"), nullable=False
    )  # offer ID
    creation_time = Column(
        "creation_time", DateTime(timezone=True), server_default=func.now()
    )

    ## ownership
    # if user_id is null, coupon_code *must* be set, otherwise the coupon is useless
    user_id = Column(
        Integer, ForeignKey("users.id", ondelete="CASCADE"), nullable=True
    )  # has this coupon been claimed by a user?
    coupon_code = Column(Unicode(255), nullable=True)  # activation code for coupon

    ## status
    days_used_previously = Column(
        Integer, nullable=False, server_default="0"
    )  # days used the previous time the coupoun was active
    activation_time = Column(DateTime(timezone=True), nullable=True)


class BlankCSRFForm(FlaskForm):
    pass


def retrieve_config(key):
    entry = Config.query.filter_by(key=key).first()
    return entry.value if entry else None
