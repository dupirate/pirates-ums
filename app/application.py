# __init__.py is a special Python file that allows a directory to become
# a Python package so it can be accessed using the 'import' statement.

import base64
import os

from flask import Flask, redirect
from flask_mail import Mail
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect
from requests_oauthlib import OAuth2Session

from app.constants import OAUTH_STATE_TOKEN

# Disable ruff warning for unused and not top of file imports for this file
# ruff: noqa: E402 F401

# Setup Flask
app = Flask(__name__)  # The WSGI compliant web application object

# Load App Config settings
# Load common settings from 'app/settings.py' file
app.config.from_object("app.settings")

environment = os.environ.get("UMS_WEBSITE_ENV", "dev")

env_settings_path = app.root_path + f"/../configs/{environment}.py"
app.config.from_pyfile(env_settings_path)

# Setup Flask-SQLAlchemy -- Do this _AFTER_ app.config has been loaded
db = SQLAlchemy(app)  # Setup Flask-SQLAlchemy

# Setup Flask-Migrate
migrate = Migrate(app, db)

scope = ["https://www.googleapis.com/auth/userinfo.email", "openid"]

# setup authlib oauth
oauth_session = OAuth2Session(scope=scope)

from app.models.user_models import MyRegisterForm, User
from app.user_manager import CustomUserManager

user_manager = CustomUserManager(app, db, User, MyRegisterForm, None)

from app.views.oidc_views import oidc_auth_callback


def redirect_to_oauth_verification(redirect_url: str, email_hint: str | None = None):
    authorization_base_url = "https://accounts.google.com/o/oauth2/v2/auth"

    oauth_session.redirect_uri = app.url_for("oidc_auth_callback", _external=True)

    encoded_redirect = base64.urlsafe_b64encode(redirect_url.encode()).decode()

    state = user_manager.generate_token(OAUTH_STATE_TOKEN, encoded_redirect)
    authorization_url, _ = oauth_session.authorization_url(
        authorization_base_url,
        access_type="offline",
        prompt="select_account",
        state=state,
        login_hint=email_hint,
    )

    return redirect(authorization_url)


user_manager.redirect_to_oauth = redirect_to_oauth_verification


# Initialize Flask Application
def init_app(extra_config_settings={}):
    print("creating flask app!")
    # Read extra config settings from function parameter 'extra_config_settings'
    app.config.update(
        extra_config_settings
    )  # Overwrite with 'extra_config_settings' parameter

    # Setup Flask-Mail
    mail = Mail(app)
    mail.init_app(app)

    # Setup WTForms CsrfProtect
    app.csrf = CSRFProtect(app)

    # Define bootstrap_is_hidden_field for flask-bootstrap's bootstrap_wtf.html
    from wtforms.fields import HiddenField

    def is_hidden_field_filter(field):
        return isinstance(field, HiddenField)

    app.jinja_env.globals["bootstrap_is_hidden_field"] = is_hidden_field_filter

    # Setup an error-logger to send emails to app.config.ADMINS
    init_email_error_handler(app)

    # Setup Flask-User to handle user account related forms
    from app.models.misc_models import Config
    from app.views.misc_views import user_profile_page
    oauth_session.client_id = app.config["GOOGLE_CLIENT_ID"]

    return app


def init_email_error_handler(app: Flask):
    """
    Initialize a logger to send emails on error-level messages.
    Unhandled exceptions will now send an email message to app.config.ADMINS.
    """
    if app.debug:
        return  # Do not send error emails while developing

    # Retrieve email settings from app.config
    host = app.config["MAIL_SERVER"]
    port = app.config["MAIL_PORT"]
    from_addr = app.config["MAIL_DEFAULT_SENDER"]
    username = app.config["MAIL_USERNAME"]
    password = app.config["MAIL_PASSWORD"]
    secure = () if app.config.get("MAIL_USE_TLS") else None

    # Retrieve app settings from app.config
    to_addr_list = app.config["ADMINS"]
    subject = app.config.get("APP_SYSTEM_ERROR_SUBJECT_LINE", "System Error")

    # Setup an SMTP mail handler for error-level messages
    import logging
    from logging.handlers import SMTPHandler

    mail_handler = SMTPHandler(
        mailhost=(host, port),  # Mail host and port
        fromaddr=from_addr,  # From address
        toaddrs=to_addr_list,  # To address
        subject=subject,  # Subject line
        credentials=(username, password),  # Credentials
        secure=secure,
    )
    mail_handler.setLevel(logging.ERROR)
    app.logger.addHandler(mail_handler)


@app.cli.command("first-db-init")
def first_db_init():
    with app.app_context():
        print("[flask app] db init start")
        from app.commands.init_db_command import init_db

        init_db()
        print("[flask app] db init done")
