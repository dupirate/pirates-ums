# This file defines command line commands for manage.py
#
# Copyright 2014 SolidBuilds.com. All rights reserved
#
# Authors: Ling Thio <ling.thio@gmail.com>

import datetime
import random

from app.application import app, db
from app.models.user_models import PaymentType, User, Role
from app.models.misc_models import Config

GENERATE_FAKE_USERS: bool = False  # use for local development


# @manager.command
def init_db():
    """Initialize the database."""
    # Create all tables first
    db.create_all()

    create_users()
    create_config()


def create_users():
    """Create users"""

    # Adding roles
    admin_role = find_or_create_role("admin", "Admin")

    # Add users
    find_or_create_user(
        "Pirates", "Admin", "admin@pirates.ie", "OmibiVp9GCRZ", admin_role
    )
    # user = find_or_create_user(u'User', u'Example', u'user@example.com', 'Password1')

    if GENERATE_FAKE_USERS:
        #
        names = [
            "Maria",
            "Nushi",
            "Mohammed",
            "Jose",
            "Wei",
            "Ahmed",
            "Yan",
            "Ali",
            "John",
            "David",
            "Li",
            "Abdul",
            "Ana",
            "Ying",
            "Michael",
        ]
        for _ in range(500):
            first_name = random.choice(names)
            second_name = random.choice(names)
            # replicate the TCD email generation process
            email = (
                second_name[:6]
                + first_name[: max(1, 7 - len(second_name))]
                + "@test.internal.pirates.ie"
            )
            email = email.lower()
            _ = find_or_create_user(
                first_name, second_name, email, "correct horse battery staple"
            )

    # Save to DB
    db.session.commit()


def find_or_create_role(name, label):
    """Find existing role or create new role"""
    role = Role.query.filter(Role.name == name).first()
    if not role:
        role = Role(name=name, label=label)
        db.session.add(role)
    return role


def find_or_create_user(first_name, last_name, email, password, role=None):
    """Find existing user or create new user"""
    user = User.query.filter(User.email == email).first()
    if not user:
        user = User(
            email=email,
            first_name=first_name,
            last_name=last_name,
            password=app.user_manager.hash_password(password),
            active=True,
            last_payment_type=PaymentType.system,
            last_payment_date=datetime.datetime(1651, 1, 1),
            email_confirmed_at=datetime.datetime.utcnow(),
        )
        if role:
            user.roles.append(role)
        db.session.add(user)
    return user


def create_config():
    configs = {
        "CURRENT_MEMBERSHIP_PERIOD": "2023/24",
        "CURRENT_MEMBERSHIP_COST_EUR_CREDIT_DEBIT_CARD": "2.35",
        "CURRENT_MEMBERSHIP_COST_EUR_CASH": "2.00",
        "MAX_CONCURRENT_CONNECTIONS_PER_ACCOUNT": "5",
        "MEETING_TIME": "(time to be determined)",
        "AUTHENTICATION_API_KEY": "MGvJZRb3pgmx4nDWj7NMYtGgeZR+6Fa06aegeYZTU2M",
        "STRIPE_MEMBERSHIP_PRODUCT_SKU": "price_1OlIneGIvpqiTas2EZiGn7OF",
        "STRIPE_WEBHOOK_SECRET": "whsec_WOGDbSwCxMsiYCL98ksulUpRrnYkBODl",
    }

    for k, v in configs.items():
        if Config.query.filter_by(key=k).count() == 0:
            db.session.add(Config(key=k, value=v))
    db.session.commit()
