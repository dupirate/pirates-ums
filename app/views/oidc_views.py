import base64
import json

from flask import flash, make_response, redirect, request

from app.application import app, oauth_session
from app.constants import (
    EMAIL_COOKIE_SIGNED_TOKEN,
    EMAIL_COOKIE_TOKEN,
    OAUTH_STATE_TOKEN,
    OAUTH_TIMEOUT,
)


@app.route("/oidc/callback")
def oidc_auth_callback():
    token_url = "https://www.googleapis.com/oauth2/v4/token"
    oauth_session.redirect_uri = app.url_for("oidc_auth_callback", _external=True)

    full_url = request.url
    if app.debug:
        # pretend we are using HTTPs if we are debugging
        full_url = full_url.replace("http://", "https://")

    state = request.args.get("state")

    token_type, encoded_redirect = app.user_manager.verify_token(
        state, OAUTH_TIMEOUT.seconds
    )

    if token_type != OAUTH_STATE_TOKEN:
        flash("Invalid state token", "error")
        return redirect("/")

    redirect_url = base64.urlsafe_b64decode(encoded_redirect).decode()

    oauth_session.fetch_token(
        token_url,
        client_secret=app.config["GOOGLE_CLIENT_SECRET"],
        authorization_response=full_url,
        timeout=8,
    )

    r = oauth_session.get("https://www.googleapis.com/oauth2/v1/userinfo", timeout=20)

    user_email = None

    if r and r.status_code == 200:
        user_info = json.loads(r.content)
        print(user_info)
        if (
            "hd" in user_info
            and user_info["hd"] == "tcd.ie"
            and user_info["verified_email"]
        ):
            user_email = user_info["email"]
        else:
            non_tcd_email = user_info["email"]
            flash(
                f'Not a TCD email ("{non_tcd_email}"), please authenticate using an @tcd.ie google account.',
                "error",
            )
    else:
        flash("Internal OCID error", "error")

    resp = make_response(redirect(redirect_url))

    if user_email:
        email_token = app.user_manager.generate_token(
            EMAIL_COOKIE_SIGNED_TOKEN, user_email
        )

        resp.set_cookie(EMAIL_COOKIE_SIGNED_TOKEN, email_token, secure=not app.debug)
        resp.set_cookie(EMAIL_COOKIE_TOKEN, user_email, secure=not app.debug)

        flash(f"Verified your email as: {user_email}", "info")

    return resp
