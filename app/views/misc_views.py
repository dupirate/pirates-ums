# Copyright 2014 SolidBuilds.com. All rights reserved
#
# Authors: Ling Thio <ling.thio@gmail.com>

import csv
import datetime
import json
import random
from hmac import compare_digest
from io import StringIO
from typing import Any, Dict, List
from urllib.parse import urlencode

import jwt
import stripe
from flask import (
    Response,
    flash,
    jsonify,
    make_response,
    redirect,
    render_template,
    request,
    send_file,
    url_for,
)
from markupsafe import Markup 
from flask_user import current_user, login_required, roles_accepted

from app.application import app, db, redirect_to_oauth_verification
from app.models.misc_models import BlankCSRFForm, Config, Offer, retrieve_config
from app.models.user_models import (
    AddUserForm,
    DeleteUserConfirmForm,
    EditOrCreateOffer,
    EditUserForm,
    PaymentType,
    StripePaymentForm,
    User,
    UserProfileForm,
)

stripe_keys = app.config["STRIPE_KEYS"]

try:
    import app.myemma_manager as myemma_manager
except Exception as e:
    print("Unable to load MyEmma manager, some functionality will not work")
    print(e)


# The Home page is accessible to anyone
@app.route("/")
def home_page():
    redirected = request.values.get("from-pirates-ie") is not None
    return render_template("pages/home_page.html", has_been_redirected=redirected)

@app.route("/links")
def links():
    return render_template("pages/links.html")


@app.route("/user/register/verify_email")
def verify_email_for_registration():
    return redirect_to_oauth_verification("/user/register")


def build_sso_url(app_config: Dict[str, Any], state: str | None) -> str:
    user_info = {
        "ums_user_id": current_user.id,
        "is_admin": current_user.is_admin,
        "exp": datetime.datetime.now(tz=datetime.timezone.utc)
        + datetime.timedelta(minutes=5),
    }

    if app_config.get("share_email", False):
        user_info["email"] = current_user.email

    user_token = jwt.encode(
        user_info, key=app_config["shared_secret"], algorithm="HS512"
    )

    query = {"user": user_token}

    if state:
        query["state"] = state

    url = app_config["redirect_url"] + "?" + urlencode(query)

    return url


@app.route("/sso")
@login_required
def pirates_sso_endpoint():
    sso_config: Dict[str, Dict[str, Any]] = app.config["SSO_ENDPOINTS"]

    app_id = request.args.get("app")
    state = request.args.get("state")

    if app_id not in sso_config.keys():
        flash(f"Unknown SSO application {app_id}", "error")
        return redirect("/")
    app_config = sso_config[app_id]

    full_member_only = app_config.get("full_member_only", True)

    if full_member_only and not current_user.fully_paid:
        app_name = app_config.get("name", app_id)
        flash(
            Markup(
                f"You must pay your membership fee before you can access the {app_name}, please do so now on the <i>membership</i> tab."
            ),
            "warning",
        )
        return redirect(url_for("membership_status"))

    callback_url = build_sso_url(app_config, state)

    return redirect(callback_url)


# Privacy policy
@app.route("/privacy-policy")
def privacy_policy():
    return render_template("pages/privacy_policy.html", title="Privacy Policy")


@app.route("/privacy-policy/printable")
def privacy_policy_printable():
    return render_template(
        "pages/privacy_policy.html",
        title="Dublin University Pirate Party - Privacy Policy",
        printable=True,
    )


# The User page is accessible to authenticated users (users that have logged in)
@app.route("/vpn_setup")
@login_required  # Limits access to authenticated users
def user_page():
    return render_template("pages/user_page.html")


@app.route("/admin/")
@roles_accepted("admin")
def admin_page():
    return redirect(url_for("member_list"))


# The Admin page is accessible to users with the 'admin' role
@app.route("/admin/member-list/")
@roles_accepted("admin")  # Limits access to users with the 'admin' role
def member_list():
    users = User.query.order_by(User.id).all()

    def format_date(d):
        return "{:%Y-%m-%d %H:%M}".format(d) if d else "-"

    return render_template("pages/member_list.html", users=users, fd=format_date)


# The Admin page is accessible to users with the 'admin' role
@app.route("/admin/offer-list/")
@roles_accepted("admin")  # Limits access to users with the 'admin' role
def offer_list():
    offers = Offer.query.order_by(Offer.id).all()
    return render_template("pages/offer_list.html", offers=offers)


# The Admin page is accessible to users with the 'admin' role
@app.route("/admin/offer-list/edit/<int:offer_id>/", methods=["GET", "POST"])
@roles_accepted("admin")  # Limits access to users with the 'admin' role
def edit_offer(offer_id):
    offer = Offer.query.get(offer_id)
    form = EditOrCreateOffer(obj=offer)

    if request.method == "POST" and form.validate():
        form.populate_obj(offer)
        db.session.commit()
        return redirect(url_for("offer_list"))

    return render_template("pages/edit_offer.html", form=form, offer=offer)


# The Admin page is accessible to users with the 'admin' role
@app.route("/admin/offer-list/new/", methods=["GET", "POST"])
@roles_accepted("admin")  # Limits access to users with the 'admin' role
def create_new_offer():
    form = EditOrCreateOffer()

    if request.method == "POST" and form.validate():
        new_offer = Offer()
        form.populate_obj(new_offer)
        db.session.add(new_offer)
        db.session.commit()
        return redirect(url_for("offer_list"))

    form.name.data = "Offer_" + datetime.datetime.now().strftime("%Y-%m-%d")
    form.description.data = "A promotional offer!"
    form.total_days.data = 30
    form.resumable.data = False

    return render_template("pages/edit_offer.html", form=form)


# The Admin page is accessible to users with the 'admin' role
@app.route("/admin/edit-user/<int:user_id>/", methods=["GET", "POST"])
@roles_accepted("admin")  # Limits access to users with the 'admin' role
def edit_user(user_id):
    user: User = User.query.get(user_id)
    form = EditUserForm(obj=user)

    if request.method == "POST" and form.validate():
        user_already_paid = user.fully_paid
        form.populate_obj(user)
        if form.fully_paid.data and not user_already_paid:
            user.last_payment_date = datetime.datetime.utcnow()
            user.last_payment_type = PaymentType.admin
        db.session.commit()
        return redirect(url_for("admin_page"))

    return render_template("pages/edit_user.html", form=form, user=user)


# The Admin page is accessible to users with the 'admin' role
@app.route("/admin/delete-user/<int:user_id>/", methods=["GET", "POST"])
@roles_accepted("admin")  # Limits access to users with the 'admin' role
def delete_user(user_id):
    user = User.query.get(user_id)
    form = DeleteUserConfirmForm(obj=user)

    if request.method == "POST" and form.validate():
        db.session.delete(user)
        db.session.commit()
        return redirect(url_for("admin_page"))

    return render_template("pages/delete_user_confirm.html", form=form, user=user)


# The Admin page is accessible to users with the 'admin' role
@app.route("/admin/add-or-renew-user", methods=["GET", "POST"])
@roles_accepted("admin")  # Limits access to users with the 'admin' role
def add_or_renew_user():
    form = AddUserForm()

    if request.method == "POST" and form.validate():
        new_user = User()
        form.populate_obj(new_user)

        if new_user.fully_paid:
            new_user.last_payment_date = datetime.datetime.utcnow()
            new_user.last_payment_type = PaymentType.admin
            try:
                myemma_manager.add_single_user(new_user.email.lower())
            except Exception:
                flash("Failed to update user on MyEmma", "error")

        # If a user by that email address already exists in the db,
        # just update their details
        existing_user: User = User.query.filter_by(email=new_user.email).first()
        if existing_user:
            user_already_paid = existing_user.fully_paid
            form.populate_obj(existing_user)
            # update payment info
            if not user_already_paid and form.fully_paid.data:
                existing_user.last_payment_date = datetime.datetime.utcnow()
                existing_user.last_payment_type = PaymentType.admin
            # update the db
            db.session.add(existing_user)
            db.session.commit()

            flash("Successfully renewed user " + existing_user.email, "success")
            return render_template("pages/add_user_success.html", renewed=True)

        # make the new user active - important for some reason?
        # (erykamil 2024) yet another reason to not use flask-user
        new_user.active = True

        # set a random password
        new_user.password = app.user_manager.hash_password(
            "%030x" % random.randrange(16**30)
        )

        # Save the user
        db.session.add(new_user)
        db.session.commit()

        # Send the new user a Password Reset message
        app.user_manager.email_manager.send_reset_password_email(new_user, None)

        flash("Account successfully created for " + new_user.email, "success")

        return render_template("pages/add_user_success.html", renewed=False)

    return render_template("pages/add_user.html", form=form)


@app.route("/admin/bulk-operations", methods=["GET"])
@roles_accepted("admin")
def bulk_operations():
    return render_template("pages/bulk_operations.html")


@app.route("/admin/mark-all-unpaid", methods=["GET"])
@roles_accepted("admin")
def mark_all_unpaid():
    if request.args.get("confirm", False):
        flash("Successfully marked all users as unpaid.", "success")
        users: List[User] = User.query.all()
        for user in users:
            user.fully_paid = False
            db.session.add(user)
        db.session.commit()
        return redirect(url_for("member_list"))
    return render_template("pages/mark_all_unpaid_confirm.html")


@app.route("/admin/delete-all-unverified", methods=["GET"])
@roles_accepted("admin")
def delete_all_unverified():
    if request.args.get("confirm", False):
        deleted_count = 0
        users: List[User] = User.query.all()
        for user in users:
            if user.email_confirmed_at is None:
                db.session.delete(user)
                deleted_count += 1
        db.session.commit()
        flash(f"Deleted {deleted_count} users without a verified email.", "success")
        app.logger.warn(f"Deleted {deleted_count} users without a verified email")
        return redirect(url_for("member_list"))
    return render_template("pages/delete_unverified_accounts_confirm.html")


@app.route("/admin/member_list.csv", methods=["GET"])
@roles_accepted("admin")
def member_list_csv():
    users = User.query.all()
    output = StringIO()
    wr = csv.writer(output, quoting=csv.QUOTE_ALL)
    wr.writerow(["Email"])
    for u in users:
        if u.fully_paid:
            wr.writerow([u.email])

    return Response(output.getvalue(), mimetype="text/csv")


@app.route("/admin/emma/users_to_update", methods=["GET"])
@roles_accepted("admin")
def debug_myemma_users_to_update():
    missing_users = myemma_manager._get_users_to_add()

    content = json.dumps(missing_users, indent=1, default=str)

    return app.response_class(content, mimetype="application/json")


@app.route("/admin/emma/update", methods=["GET"])
@roles_accepted("admin")
def myemma_update_users():
    emails = myemma_manager.update_users()

    app.logger.info(f"Added users to my emma: {emails}")

    flash(f"Added {len(emails)} users to myEmma.")

    return redirect(url_for("admin_page"))


@app.route("/admin/modify_config", methods=["GET", "POST"])
@roles_accepted("admin")
def modify_config():
    freshers_login_user = retrieve_config("FRESHERS_USER")
    if not freshers_login_user:
        db.session.add(Config(key="FRESHERS_USER", value="freshers"))
        db.session.add(Config(key="FRESHERS_PASSWD", value="ArrhMaties23!"))

    configs = Config.query.order_by(Config.key).all()
    form = BlankCSRFForm(None)
    if request.method == "POST":
        print(request.values)
        for key, value in request.values.items():
            if key == "csrf_token":
                continue
            config = Config.query.filter_by(key=key).first()
            config.value = value
            db.session.add(config)
            db.session.commit()
        return render_template("pages/modify_config.html", configs=configs, form=form)

    return render_template("pages/modify_config.html", configs=configs, form=form)


@app.route("/admin/auth_api_keys", methods=["GET", "POST"])
@roles_accepted("admin")
def auth_api_keys():
    return render_template("pages/auth_api_keys.html")


@app.route("/pages/profile", methods=["GET", "POST"])
@login_required
def user_profile_page():
    # Initialize form
    form = UserProfileForm(obj=current_user)

    # Process valid POST
    if request.method == "POST" and form.validate():
        # Copy form fields to user_profile fields
        form.populate_obj(current_user)

        # Save user_profile
        db.session.commit()

        # Redirect to home page
        return redirect(url_for("home_page"))

    # Process GET or invalid POST
    return render_template("pages/user_profile_page.html", form=form)


@app.route("/pages/profile/export", methods=["GET"])
@login_required
def export_user_data():
    profile_data = {
        "name": [current_user.first_name, current_user.last_name],
        "email": current_user.email,
        "email_confirmed_at": current_user.email_confirmed_at,
        "has_paid_for_membership": current_user.fully_paid,
        "last_payment_date": current_user.last_payment_date,
        "last_payment_type": current_user.last_payment_type,
    }

    content = json.dumps(profile_data, indent=1, default=str)

    return app.response_class(content, mimetype="application/json")


@app.route("/membership/", methods=["GET"])
@login_required
def membership_status():
    form = StripePaymentForm(None)
    pricing = {
        "card": retrieve_config("CURRENT_MEMBERSHIP_COST_EUR_CREDIT_DEBIT_CARD"),
        "cash": retrieve_config("CURRENT_MEMBERSHIP_COST_EUR_CASH"),
        "membership_period": retrieve_config("CURRENT_MEMBERSHIP_PERIOD"),
        "meeting_time": retrieve_config("MEETING_TIME"),
    }
    stripe_membership_product_sku = retrieve_config("STRIPE_MEMBERSHIP_PRODUCT_SKU")

    return render_template(
        "pages/membership_status.html",
        form=form,
        stripe_p_key=stripe_keys["p_key"],
        pricing=pricing,
        membership_product_sku=stripe_membership_product_sku,
    )


@app.route("/membership/payment/success")
def payment_success():
    flash("Payment received. You will receive an email receipt.", "success")
    return redirect(url_for("membership_status"))


@app.route("/membership/payment/canceled")
def payment_failure():
    flash(
        "Unable to complete payment. Your card has not been charged. \
            Please contact info@pirates.ie for assistance.",
        "error",
    )
    return redirect(url_for("membership_status"))


@app.route("/stripe/checkout_session_completed", methods=["POST"])
@app.csrf.exempt
def stripe_checkout_session_completed():
    payload = request.data.decode("utf-8")
    received_sig = request.headers.get("Stripe-Signature", None)

    webhook_secret = retrieve_config("STRIPE_WEBHOOK_SECRET")

    try:
        event = stripe.Webhook.construct_event(payload, received_sig, webhook_secret)
    except ValueError:
        print("Error while decoding event!")
        return "Bad payload", 400
    except stripe.error.SignatureVerificationError:
        print("Invalid signature!")
        return "Bad signature", 400

    if event.type == "checkout.session.completed":
        email = event.data.object.customer_email
        try:
            user = User.query.filter_by(email=email).first()
            user.fully_paid = True
            user.last_payment_date = datetime.datetime.utcnow()
            user.last_payment_type = PaymentType.card
            db.session.commit()

            try:
                myemma_manager.add_single_user(email.lower())
            except Exception as e:
                app.logger.warn(f"Unable to add user {email} to myEmma")
                app.log_exception(e)

        except:
            app.logger.error(f"Unable to process transaction confirmation: {event}")
            raise

    return "OK", 200


@app.route("/api/authenticate", methods=["POST"])
@app.csrf.exempt
def authenticate():
    try:
        api_key = retrieve_config("AUTHENTICATION_API_KEY")
        freshers_login_user = retrieve_config("FRESHERS_USER")
        freshers_login_password = retrieve_config("FRESHERS_PASSWD")

        if not compare_digest(request.values["api_key"], api_key):
            return jsonify(
                result="success"
            )  # return success if the api key is bad (avoids downtime during key rotation/leaking any info)

        if freshers_login_user and freshers_login_password:
            user_name = request.values["email"].strip()
            is_username_freshers = compare_digest(user_name, freshers_login_user)
            is_password_freshers = compare_digest(
                request.values["password"], freshers_login_password
            )

            # temporary login for freshers week so people can check if the VPN is working without signing up
            if is_password_freshers and is_username_freshers:
                return jsonify(result="success")

        user, user_email = app.user_manager.db_manager.get_user_and_user_email_by_email(
            request.values["email"]
        )
        if not user:
            return jsonify(result="fail")

        password_hash = user.password
        if (
            app.user_manager.verify_password(request.values["password"], password_hash)
            and user.fully_paid
        ):
            return jsonify(result="success")

        return jsonify(result="fail")
    except Exception:
        app.logger.error("internal error in vpn-authenticate, returning success")
        return jsonify(
            result="success"
        )  # return success on internal error (avoid downtime for vpn)
