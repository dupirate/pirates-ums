import datetime

EMAIL_COOKIE_SIGNED_TOKEN = "verified_email_signed"
EMAIL_COOKIE_TOKEN = "verified_email"
EMAIL_COOKIE_TTL = datetime.timedelta(hours=2)

OAUTH_STATE_TOKEN = "__oauth_state_token__"
OAUTH_TIMEOUT = datetime.timedelta(hours=1)
