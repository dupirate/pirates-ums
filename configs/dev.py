# *****************************
# Environment specific settings
# *****************************

# DO NOT use "DEBUG = True" in production environments
DEBUG = True

# DO NOT use Unsecure Secrets in production environments
# Generate a safe one with:
#     python -c "import os; print repr(os.urandom(24));"
SECRET_KEY = "A Pirate Secret Key"

# SQLAlchemy settings
SQLALCHEMY_DATABASE_URI = "sqlite:///test.db"
SQLALCHEMY_TRACK_MODIFICATIONS = False  # Avoids a SQLAlchemy Warning

# Flask-Mail settings
# For smtp.gmail.com to work, you MUST set "Allow less secure apps" to ON in Google Accounts.
# Change it in https://myaccount.google.com/security#connectedapps (near the bottom).
MAIL_SERVER = "smtp.gmail.com"
MAIL_PORT = 587
MAIL_USE_SSL = False
MAIL_USE_TLS = True
MAIL_USERNAME = "no-reply@pirates.ie"
MAIL_PASSWORD = "(redacted)"
MAIL_DEFAULT_SENDER = '"DU Pirates (no-reply)" <no-reply@pirates.ie>'

GOOGLE_CLIENT_ID = (
    "636390819511-ghhr87hnmi87g33a3lk7oq8ebqms43pm.apps.googleusercontent.com"
)
GOOGLE_CLIENT_SECRET = "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"

ADMINS = ['"Pirate Party" <pirate@csc.tcd.ie>']
STRIPE_KEYS = {"p_key": "pk_test_S23Pv8H9bDNKz0UUMAL83Lbf"}

# internal pirate SSO endpoints
SSO_ENDPOINTS = {
    "ctf": {
        "shared_secret": "sso_key_dev_MXIpIPWGW3EZBryZmgEpqA8tVymF7hfby",
        "redirect_url": "http://localhost:5001/sso/callback",
        "full_member_only": True,
    }
}
