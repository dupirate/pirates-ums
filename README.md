# Pirates Website

A user management system for the Dublin University Pirate Party, using the Flask web framework. It is also the location of the website.

## Getting started

The first step is to create your GitLab account (obviously). Once this is done, one of the current maintainers can add the new account. Next, simply clone the repo to whatever location you want on your computer. Development requires having python and pip installed. Version 2 or 3 is fine, which ever you prefer. To install the dependencies in the traditional manner, run:

    pip install -r requirements.txt

From the project root.

Alternatively, if you prefer to use python `venv`, then use:

    python -m venv .venv
    source .venv/bin/activate
    pip install -r requirements.txt

Again from the project root. This will provide the required python packages in a `venv`, so they will only be available for the current shell session. This allows one to avoid cluttering their global python install with project specific development packages.

## Connecting to the server

### Windows

First, download and install [PuTTY](https://www.putty.org/). Make sure you also get PuTTYgen. Then, create yourself a public/private keypair using PuTTYgen. Make an RSA key of 2048 bits. Save the file to your .ssh folder, usually:

    C:\Users\{User}\.ssh

Send the public key part to one of the maintainers. They will add it to the authorized_keys file in the .ssh/ folder on the server.

Next, set up your PuTTY connection. Everything is the same as the defaults except:

    Hostname: 178.62.40.52
    Type: SSH
    Connection/SSH/Auth/Private key for authentication: path to your key

Then click open and enter your password, and you should be connected.

### Linux

Simply ssh into the server with root@178.62.40.52 (assuming the DigitalOcean IP address hasn't changed)

## Stop it, pull it, start it, ~~bop it~~

Here is the standard workflow for making a change to the website:

1. Commit a change to master.
2. Open up **two** ssh windows
3. In the first one, remain as root user. In the second one, switch user to pirates-ums

   `su pirates-ums`

4. In the root user, stop the service

   `service piratesums stop`

5. In the pirates-ums user, navigate to the project folder and pull the changes from master
6. Back in the root user, start the service again

   `service piratesums start`

And that's it!

## Local dev

Of course, you may want to do some local testing, rather than debugging in prod. To do this, you'll need a file called `local_settings.py`. There's one on the prod server, so just grab that and change the `sqlalchemy_database_uri` to

    mysql://{local_file_.db}

or

    sqlite:///test.db

The file `local_settings.py` must be placed in `./app/local_setting.py`, respective to the root directory.

We wouldn't want to accidentally delete all the data in the production database! Once you have started a development SQL database and pointed the `sqlalchemy_database_uri` to it correctly the db can be initialized for first use by running

    flask first-db-init

One cannot register a new account on a local development server unless you set up an email server to send registration emails. This is far too much work to be bothered with, so the above `flask first-db-init` creates an admin user to log in to the site with. The credentials of this admin user, and indeed everything the command does is defined in `./app/commands/init_db_command.py`.

A development webpage can be activated using

    flask run

And it should spin up a local instance of pirates webpage for you to see development changes reflected in.

You may get an error: `ModuleNotFoundError: No module named 'MySQLdb'`. In this case, a solution on Linux is to run the following commands:

    sudo apt install libmysqlclient-dev
    pip3 install mysqlclient

For other OSes, if a solution is found please update this README.

## Misc

[sqlitebrowser](https://github.com/sqlitebrowser/sqlitebrowser) can be quite useful for visually interacting with and exploring the SQL database. Other, similar programs are available.

This repo uses a git submodule to pull in the EmmaPython code. This code can be found in `./thirdparty/EmmaPython`. To update this, and any other git modules the easiest command is `git submodule foreach git pull origin master`. This will update every submodule to the latest commit on the master branch. This will obviously have problems if some of the submodules use main as their default branch. In that scenario, please consult [the git book](https://git-scm.com/book/en/v2/Git-Tools-Submodules) for more information.

## Formatting

This repo uses the following formatters:
[`alejandra`](https://github.com/kamadorueda/alejandra), [`deadnix`](https://github.com/astro/deadnix) and [`statix`](https://github.com/NerdyPepper/statix) for formatting nix files.

It uses [`black`](https://github.com/psf/black) for formatting python and [`prettier`](https://prettier.io/) for formatting
`"_.cjs"`
`"_.css"`
`"_.js"`
`"_.json"`
`"_.json5"`
`"_.jsx"`
`"_.md"`
`"_.mdx"`
`"_.mjs"`
`"_.scss"`
`"_.ts"`
`"_.tsx"`
`"_.vue"`
`"_.yaml"`
`"_.yml"`
files.

We then use [`djlint`](https://github.com/djlint/djlint) to format the HTML files, as prettier struggles to properly format HTML files with templating included.

The easiest way to run these formatters correctly configured is to use nix.

## Nix

This project has a flake.nix. By using `nix develop` you will be dropped into a `devshell` that provides all system dependencies and then executes a `shellhook` to install the python requirements in a `venv`. Please see the flake file for a list of all programs it provides.

`nix fmt` and `nix flake check` have been configured to format and check the formatting of code in the repo, as defined in [Formatting](#formatting). For exact details, please see the `flake.nix` file.

## Acknowledgements

With thanks to the following Flask extensions:

- [Alembic](alembic.readthedocs.org)
- [Flask-Migrate](flask-migrate.readthedocs.org)
- [Flask-User](pythonhosted.org/Flask-User/)

[Flask-User-starter-app](https://github.com/lingthio/Flask-User-starter-app) was used as a starting point for this code repository.
